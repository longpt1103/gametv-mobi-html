/*!
 * gametv
 * 
 * 
 * @author Thanh Long Pham
 * @version 1.0.0
 * Copyright 2020. MIT licensed.
 */"use strict";

$(document).ready(function () {
  $(window).bind("scroll", function () {
    175 < $(window).scrollTop() ? $(".gametv-navbar").addClass("fixed") : $(".gametv-navbar").removeClass("fixed"), scrollFunction();
  }), $("#js-dark-mode").on("click", function () {
    $("html").toggleClass("dark-mode");
  }), $("#btnSearch").on("click", function () {
    $("#boxSearch").toggle();
  }), $(".icon-toggle").on("click", function () {
    $(this).toggleClass("current-toggle"), $(this).next().toggle();
  }), $("#showMenu").on("click", function () {
    $("#menu-mobi").slideDown(500);
  }), $("#close-menu").on("click", function () {
    $("#menu-mobi").slideUp(500);
  }), $(".menu-modal").on("click", function () {
    $("#menu-mobi").slideUp(100);
  });
});var myVar,
    slideIndex = 1;function plusSlides(e) {
  clearRunSlide(), showSlides(slideIndex += e);
}function showSlides(e) {
  var n,
      t = $(".item"),
      i = $(".dot");for ((e > t.length || slideIndex > t.length) && (slideIndex = 1), (e < 1 || slideIndex < 1) && (slideIndex = t.length), n = 0; n < t.length; n++) {
    t[n].className = "item", i[n].className = "dot";
  }1 === slideIndex && (t[t.length - 1].className = "item item-active item-prev", t[slideIndex - 1].className = "item item-active item-center", t[slideIndex].className = "item item-active item-next"), slideIndex === t.length && (t[slideIndex - 2].className = "item item-active item-prev", t[slideIndex - 1].className = "item item-active item-center", t[0].className = "item item-active item-next"), slideIndex !== t.length && 1 !== slideIndex && (t[slideIndex - 2].className = "item item-active item-prev", t[slideIndex - 1].className = "item item-active item-center", t[slideIndex].className = "item item-active item-next"), i[slideIndex - 1].className += " active", autoRunSlide();
}function autoRunSlide() {
  myVar = setTimeout(function () {
    showSlides(slideIndex++);
  }, 5e3);
}function clearRunSlide() {
  clearTimeout(myVar);
}function currentSlide(e) {
  clearRunSlide(), showSlides(slideIndex = e);
}showSlides(slideIndex);var slider = document.getElementsByClassName("slider__content")[0];slider.addEventListener("touchstart", handleTouchStart, !1), slider.addEventListener("touchmove", handleTouchMove, !1);var xDown = null,
    yDown = null;function handleTouchStart(e) {
  xDown = e.touches[0].clientX, yDown = e.touches[0].clientY;
}function handleTouchMove(e) {
  var n, t, i, l;xDown && yDown && (n = e.touches[0].clientX, t = e.touches[0].clientY, i = xDown - n, l = yDown - t, Math.abs(i) > Math.abs(l) && plusSlides(0 < i ? -1 : 1), yDown = xDown = null);
}var mybutton = document.getElementById("backTop");function topFunction() {
  document.body.scrollTop = 0, document.documentElement.scrollTop = 0;
}function scrollFunction() {
  20 < document.body.scrollTop || 20 < document.documentElement.scrollTop ? mybutton.style.display = "block" : mybutton.style.display = "none";
}