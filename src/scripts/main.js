// window.GapoScript = (() => {
//   const utils = {
//     isMobile: (agent) =>
//       /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(
//         agent || window.navigator.userAgent
//       ),
//     fixIE: () => {
//       if (navigator.userAgent.match(/IEMobile\/10\.0/)) {
//         const msViewportStyle = document.createElement("style");
//         msViewportStyle.appendChild(
//           document.createTextNode("@-ms-viewport{width:auto!important}")
//         );
//         document.querySelector("head").appendChild(msViewportStyle);
//       }
//     },
//   };

//   const initSearchBox = () => {
//     const $searchInput = $("#js-search-input");
//     const $searchBox = $("#js-search-box");

//     $searchInput
//       .on("focus", () => {
//         $searchBox.addClass("header__search-box--focus");
//       })
//       .on("blur", () => {
//         $searchBox.removeClass("header__search-box--focus");
//       });
//   };
//   const handleDarkMode = () => {
//     const $DarkMode = $("#js-dark-mode");
//     $("#js-dark-mode").on("click", () => {
//       $("html").toggleClass("dark-mode");
//     });
//   };
//   const handleSearch = () => {
//     const $btnSearch = $("#btnSearch");
//     $("#btnSearch").on("click", () => {
//       $("#boxSearch").toggle();
//     });
//   };
//   const toggleMenu = () => {
//     const $btnMenuToggle = $(".icon-toggle");
//     $(".icon-toggle").on("click",() => {
//       $(this).toggleClass("current-toggle");
//       $(this).next().toggle();
//     });
//   };
//   const showMenu = () => {
//     const $btnShowMenu = $("#showMenu");
//     const $btnHideMenu = $("#close-menu");
//     const $btnMenuModal = $(".menu-modal");
//     $("#showMenu").on("click",() => {
//       $('#menu-mobi').slideDown(500);
//     });
//     $("#close-menu").on("click",() => {
//       $('#menu-mobi').slideUp(500);
//     });
//     $(".menu-modal").on("click",() => {
//       $('#menu-mobi').slideUp(100);
//     });
//   };
  

//   const init = () => {
//     /* Fix IE */
//     utils.fixIE();

//     initSearchBox();
//     handleDarkMode();
//     handleSearch();
//     toggleMenu();
//     showMenu();
//   };

//   return {
//     init,
//   };
// })();

// $(document).ready(() => {
//   window.GapoScript.init();
// });

$(document).ready(function(){
  $(window).bind("scroll", function () {
    if ($(window).scrollTop() > 175) {
      $(".gametv-navbar").addClass("fixed");
    } else {
      $(".gametv-navbar").removeClass("fixed");
    }
    scrollFunction();
  });
  $("#js-dark-mode").on("click", function(){
    $("html").toggleClass("dark-mode");
  });
  $("#btnSearch").on("click", function(){
    $("#boxSearch").toggle();
  });
  $(".icon-toggle").on("click",function(){
    $(this).toggleClass("current-toggle");
    $(this).next().toggle();
  });
  $("#showMenu").on("click",function(){
    $('#menu-mobi').slideDown(500);
  });
  $("#close-menu").on("click",function(){
    $('#menu-mobi').slideUp(500);
  });
  $(".menu-modal").on("click",function(){
    $('#menu-mobi').slideUp(100);
  });
  
  
  
});
var slideIndex = 1;
  showSlides(slideIndex);

  // Next/previous controls
  function plusSlides(n) {
    clearRunSlide();
    showSlides((slideIndex += n));
  }

  // Thumbnail image controls
  

  function showSlides(n) {
    var i;
    var slides = $(".item");
    var dots = $(".dot");
    // slideIndex++;
    if (n > slides.length || slideIndex > slides.length) {
      slideIndex = 1;
    }
    if (n < 1 || slideIndex < 1) {
      slideIndex = slides.length;
    }
    for (i = 0; i < slides.length; i++) {
      slides[i].className = "item";
      dots[i].className = "dot";
    }
    if (slideIndex === 1) {
      slides[slides.length - 1].className = "item item-active item-prev";
      slides[slideIndex - 1].className = "item item-active item-center";
      slides[slideIndex].className = "item item-active item-next";
    }
    if (slideIndex === slides.length) {
      slides[slideIndex - 2].className = "item item-active item-prev";
      slides[slideIndex - 1].className = "item item-active item-center";
      slides[0].className = "item item-active item-next";
    }
    if (slideIndex !== slides.length && slideIndex !== 1) {
      slides[slideIndex - 2].className = "item item-active item-prev";
      slides[slideIndex - 1].className = "item item-active item-center";
      slides[slideIndex].className = "item item-active item-next";
    }

    dots[slideIndex - 1].className += " active";
    autoRunSlide();
    // setTimeout(showSlides(slideIndex + 1), 3000);
  }

  var myVar;

  function autoRunSlide() {
    myVar = setTimeout(function () {
      var newSlideIndex = slideIndex++;
      showSlides(newSlideIndex);
    }, 5000);
  }

  function clearRunSlide() {
    clearTimeout(myVar);
  }
function currentSlide(n) {
    clearRunSlide();
    showSlides((slideIndex = n));
  }

var slider = document.getElementsByClassName('slider__content')[0];
slider.addEventListener('touchstart', handleTouchStart, false);        
slider.addEventListener('touchmove', handleTouchMove, false);

var xDown = null;                                                        
var yDown = null;                                                        

function handleTouchStart(evt) {                                         
    xDown = evt.touches[0].clientX;                                      
    yDown = evt.touches[0].clientY;                                      
};                                                

function handleTouchMove(evt) {
    if ( ! xDown || ! yDown ) {
        return;
    }

    var xUp = evt.touches[0].clientX;                                    
    var yUp = evt.touches[0].clientY;

    var xDiff = xDown - xUp;
    var yDiff = yDown - yUp;

    if ( Math.abs( xDiff ) > Math.abs( yDiff ) ) {/*most significant*/
        if ( xDiff > 0 ) {
            /* left swipe */ 
            plusSlides(-1);
        } else {
            /* right swipe */
            plusSlides(+1);
        }                       
    } else {
        if ( yDiff > 0 ) {
            /* up swipe */ 
        } else { 
            /* down swipe */
        }                                                                 
    }
    /* reset values */
    xDown = null;
    yDown = null;                                             
};
// backtotop
var mybutton = document.getElementById("backTop");

function topFunction() {
  document.body.scrollTop = 0;
  document.documentElement.scrollTop = 0;
}
function scrollFunction() {
  if (document.body.scrollTop > 20 || document.documentElement.scrollTop > 20) {
    mybutton.style.display = "block";
  } else {
    mybutton.style.display = "none";
  }
}